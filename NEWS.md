# mesoutils 0.2.0

* Added Fake data to explain and illustrate the goal of the package

# mesoutils 0.1.0

* Added the function `get_mean_data` to the package

# mesoutils 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
