
<!-- README.md is generated from README.Rmd. Please edit that file -->

# mesoutils

<!-- badges: start -->
<!-- badges: end -->

The goal of mesoutils is to get information about data frame

## Installation

You can install the development version of mesoutils like so:

``` r
devtools::install_local("mesoutils_0.2.0.tar.gz")
```

## A data frame to illustrate the package

We give you some simple data in order to illustrate the package’s
function:

``` r
library(mesoutils)
data_mesoutils
#> # A tibble: 10 × 5
#>     num1  num2     num3 fac1   char1
#>    <int> <int>    <dbl> <fct>  <chr>
#>  1     1     4 -0.160   Blue   a    
#>  2     2     3  1.18    Red    b    
#>  3     3     5 -0.617   Yellow c    
#>  4     4     6  0.335   Blue   d    
#>  5     5    10 -0.664   Blue   e    
#>  6     6     1  0.00804 Yellow f    
#>  7     7     2 -0.500   Yellow g    
#>  8     8     9 -0.447   Blue   h    
#>  9     9     7  2.03    Blue   i    
#> 10    10     8  0.782   Yellow j
```

## Get information about a data frame with the `get_info_data` function:

``` r
get_info_data(data_mesoutils)
#> $dimension
#> [1] 10  5
#> 
#> $names
#> [1] "num1"  "num2"  "num3"  "fac1"  "char1"
```

## Get the mean of all numeric variables with the `get_mean_data` function:

``` r
get_mean_data(data_mesoutils)
#> Warning: Predicate functions must be wrapped in `where()`.
#> 
#>   # Bad
#>   data %>% select(is.numeric)
#> 
#>   # Good
#>   data %>% select(where(is.numeric))
#> 
#> ℹ Please update your code.
#> This message is displayed once per session.
#> # A tibble: 1 × 3
#>    num1  num2  num3
#>   <dbl> <dbl> <dbl>
#> 1   5.5   5.5 0.194
```
