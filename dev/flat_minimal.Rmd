---
title: "flat_get_info_data.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(glue)
library(dplyr)
```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# My function

```{r function-get_info_data}
#' A function to get information from data
#' 
#' This function return the dimensions and the names of the data
#'
#' @importFrom glue glue
#' @param data data frame. The data frame of your interest
#'
#' @return a list with 2 elements. First the dimension and then the names
#' @export
#'
#' @examples
get_info_data <- function(data = iris) {
  if(isTRUE(inherits(data,"data.frame"))){
    dimension = dim(data)
    noms = names(data)    
  }else{
    stop(glue("{substitute(data)} is not a data frame"))
  }

  return(list(dimension=dimension,names=noms))
}
```

```{r examples-get_info_data}
get_info_data()
get_info_data(data_mesoutils)
```

```{r tests-get_info_data}
test_that("get_info_data works", {
  expect_true(inherits(get_info_data,"function"))
  expect_equal(get_info_data(iris),
               expected = list(
                 dimension=dim(iris),
                 names=names(iris)
               ))

})

test_that("get_info_data does not works", {
  expect_error(get_info_data("mydata"),
               regexp = "mydata is not a data frame")

})
```

# get_mean_data
    
```{r function-get_mean_data}
#' Mean of numeric variable
#' 
#' Get the mean of the numeric variables of data frame
#' 
#' @importFrom dplyr summarise across select
#' 
#' @param data data frame. The data frame of your interest
#'
#' @return a data frame containing the mean of each numeric variable 
#' 
#' @export
get_mean_data <- function(data){
  if(isTRUE(data %>% select(is.numeric) %>% length() > 0)){
  temp <- data %>%
    summarise(across(.cols = is.numeric,.fns = mean))    
  }else{
    stop("You must have numeric data in your data frame")
  }
  return(temp)
}
```
  
```{r example-get_mean_data}
get_mean_data(data=iris)
```
  
```{r tests-get_mean_data}
test_that("get_mean_data works", {
  library(dplyr)
  expect_true(inherits(get_mean_data, "function")) 
  expect_equal(
    object = get_mean_data(data=iris),
    expected = iris %>%
    summarise(across(.cols = is.numeric,.fns = mean))
  )
})

test_that("get_mean_data does not work", {
  # library(dplyr)
  expect_error(
    object =  get_mean_data(data=iris %>% mutate_all(as.character)),
    regexp = "You must have numeric data in your data frame") 
})

```
  
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_minimal.Rmd", vignette_name = "Minimal")
```
